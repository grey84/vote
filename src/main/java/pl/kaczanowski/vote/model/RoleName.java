package pl.kaczanowski.vote.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
