package pl.kaczanowski.vote.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder


public class User {

    private Long id;
    private String login;
    private String firstName;
    private String lastName;
    private String email;
    private String password;

    private OffsetDateTime createdAt;
    private OffsetDateTime updatedAt;

    private List<Role> roleList = new ArrayList<>();

}
